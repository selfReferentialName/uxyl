// architectural definitions
// many architectures will copy this file to include inline syscalls.

#include <stdint.h>

#define PAGESIZE 4096

void *uxy__get_pages(uint32_t n);
void *uxy__drop_pages(void *p, uint32_t n);

// sane default for most architectures
// based on amd64
// convert a raw size into a size after alignment
__attribute__((always_inline)) size_t uxy__aligned_size (size_t raw) {
	if (raw <= 2) {
		return raw;
	} else if (raw <= 4) {
		return 4;
	} else if (raw <= 8) {
		return 8;
	} else if (raw % 16 == 0) {
		return raw;
	} else {
		return raw - raw % 16 + 16;
	}
}
