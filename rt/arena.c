/* arena.c
 * arena based memory management functions
 */

#include "arch.h"

#define CHUNKPAGES 9
#define CHUNKSIZE (PAGESIZE << CHUNKPAGES)

#define SPLITLEEWAY PAGESIZE

struct part {
	struct part *next;
	uint32_t npages;
	long double data[]; // largest datatype, should get good alignment
};

struct arena {
	struct arena *cont;
	struct part *parts;
	ptrdiff_t index;
	long double data[];
};

struct arena *uxy__anew (void) {
	struct arena *a = uxy__get_pages(CHUNKPAGES);
	a->cont = NULL;
	a->parts = NULL;
	a->index = ((ptrdiff_t) a->data) - ((ptrdiff_t) &a->cont);
	return a;
}

void *uxy__aalloc (struct arena *a, size_t n) {
	while (a->cont) {
		a = a->cont;
	}
	if (a->index + n > CHUNKSIZE) {
		if (a->index >= (CHUNKSIZE - SPLITLEEWAY)) {
			a->cont = uxy__anew();
			a = a->cont;
		} else {
			n += 16;
			struct part *out = uxy__get_pages(n / PAGESIZE + 1);
			out->next = a->parts;
			out->npages = n / PAGESIZE + 1;
			a->parts = out;
			return (void*) out->data;
		}
	}
	char *out = (char*) a + a->index;
	a->index += n;
	return (void*) out;
}

// recursively drop every part
static void drop_parts (struct part *p) {
	if (p->next) {
		drop_parts(p->next);
	}
	uxy__drop_pages(p, p->npages);
}

void uxy__adrop (struct arena *a) {
	// recursively hit every chunk
	if (a->cont) {
		uxy__adrop(a->cont);
	}
	if (a->parts) {
		drop_parts(a->parts);
	}
	uxy__drop_pages(a, CHUNKPAGES);
}
