use crate::lex;

#[derive(Debug)]
pub enum Expr {
	Call(Vec<Expr>),
	Block(Box<Expr>),
	Seq(Vec<Expr>),
	Nihil,
	Tok(lex::Token)
}
