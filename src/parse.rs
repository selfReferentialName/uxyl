/* parse.rs
 * simple recursive descent parser
 * currently only root constructs are supported
 *
 * TODO: turn assert! into error reporting
 */

use crate::ast::Expr;
use crate::lex;

// what things return
enum Return<'a> {
	Eof(Expr),
	RC(Expr, &'a [lex::Token]),
	CC(Expr, &'a [lex::Token]),
	Norm(Expr, &'a [lex::Token]),
	NotMe(&'a [lex::Token]), // used when the called function is innapropriate, NOT on error
}

#[exec_time]
pub fn parse (tl: &[lex::Token]) -> Expr {
	match expr(tl) {
		Eof(e) => e,
		Norm(e, _) => {panic!("parser::expr exited early"); e},
		RC(_, _) => panic!("Unmatched close round bracket"),
		CC(_, _) => panic!("Unmatched close curly bracket"),
		NotMe(_) => panic!("Unmatched close curly bracket"),
	}
}

use Return::*;

// Round containment
// Expr ::= ( Expr )
fn round (tl: &[lex::Token]) -> Return {
	match expr(tl) {
		RC(e, tlp) => match e {
			Expr::Call(_) => Norm(e, tlp),
			_ => Norm(Expr::Call(vec![e]), tlp),
		},
		_ => {assert!(2 != 2); NotMe(tl)}
	}
}

// Curly containment
// Expr ::= { Expr* }
fn curly (tl: &[lex::Token]) -> Return {
	match expr(tl) {
		CC(e, tlp) => Norm(Expr::Block(Box::new(e)), tlp),
		_ => {assert!(2 != 2); NotMe(tl)}
	}
}

// Calling
// Expr ::= Expr Expr
//
// Separation
// Expr ::= Expr ; Expr
// where the contained Exprs are as large as possible
//
// also gets head ( and {
fn expr (tl0: &[lex::Token]) -> Return {
	let mut tl = tl0;
	let mut seq = Vec::new();
	loop { // separation
		let mut call = Vec::new();
		loop { // calling
			if tl.len() == 0 {
				let ce = || {
					if call.len() == 0 {
						Expr::Nihil
					} else if call.len() == 1 {
						call.swap_remove(0)
					} else {
						Expr::Call(call)
					}
				};
				let fe = || {
					if seq.len() == 0 {
						ce()
					} else {
						seq.push(ce());
						Expr::Seq(seq)
					}
				};
				return Eof(fe());
			} else {
				match tl[0].tok {
					lex::Tok::Dat(_) => match leaf(tl) {
						Norm(e, l) => {
							call.push(e);
							tl = l;
						},
						_ => panic!("parse::leaf rejected a Leaf")
					},
					lex::Tok::RO => match round(tl) {
						Norm(e, l) => {
							call.push(e);
							tl = l;
						},
						_ => panic!("Syntax error: parse::round incorrectly terminated")
					},
					lex::Tok::CO => match curly(tl) {
						Norm(e, l) => {
							call.push(e);
							tl = l;
						},
						_ => panic!("Syntax error: parse::curly incorrectly terminated")
					},
					_ => {
						let ce = || {
							if call.len() == 0 {
								Expr::Nihil
							} else if call.len() == 1 {
								call.swap_remove(0)
							} else {
								Expr::Call(call)
							}
						};
						match tl[0].tok {
							lex::Tok::Semi => {seq.push(ce()); break;},
							_ => {
								let fe = || {
									if seq.len() == 0 {
										ce()
									} else {
										seq.push(ce());
										Expr::Seq(seq)
									}
								};
								match tl[0].tok {
									lex::Tok::RC => return RC(fe(), tl),
									lex::Tok::CC => return CC(fe(), tl),
									_ => panic!("I hate rust")
								}
							}
						}
					}
				}
			}
		}
	}
}

// Leaf
// Expr ::= Id | Ilit | Slit | Blit | ...
fn leaf (tl: &[lex::Token]) -> Return {
	match tl[0].tok {
		lex::Tok::Dat(_) => Norm(Expr::Tok(tl[0].clone()), &tl[1..]),
		_ => NotMe(tl)
	}
}
