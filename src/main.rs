mod lex;
mod error;
mod ast;
mod parse;
use std::fs::File;
use std::io::Read;
#[macro_use]
extern crate exec_time;

fn dump_toks(s: &str) {
	for t in lex::tokenise(s) {
		lex::dumptok(t);
	}
}

fn main() {
	let mut f = File::open("in.xyl").unwrap();
	let mut s = String::new();
	f.read_to_string(&mut s);
	let tl = lex::tokenise(&s);
	let e = parse::parse(&tl);
//	dump_toks(&s);
	println!("{:?}", e);
}
