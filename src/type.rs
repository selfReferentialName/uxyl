/* type.rs
 * the type system, compile and runtime info, and checker
 */

use crate::ast::Expr;

/// as the type system, we can't use stuff codegened elsewhere
pub enum UserFunc

/// a type
/// contains fields for user code
pub enum Type {
	op: Expr,
	size: Option<UserFunc>,
	Check: Option<UserFunc>
}

pub fn 
