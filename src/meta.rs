/* meta.rs
 * metaprogramming facilities
 * mostly getting exprs to and from an llvm suited format
 */

pub use llvm_sys::prelude::*;

// a bunch of constants to mark the size of 
const CALL: u32 = 0;
const BLOCK: u32 = 1;
const SEQ: u32 = 2;
const NIHIL: u32 = 3;
const ID: u32 = 8;
const SLIT: u32 = 9;
const BLIT: u32 = 10;
const ILIT: u32 = 11;
