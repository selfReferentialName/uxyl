/* src/lex.rs
 * 
 * parse the input into tokens
 * this is arguably a bigger job than the parser
 */

use crate::error;
use std::fmt;

// From a parsing perspective, there are five types of tokens.
// The first four are punctuation and comprise of the open and close
// curly and round brackets.
// The final is data, and it comprises of all identifiers and literals.
// This is, of course, quite a diverse set of things, so we further subdivide
// these for codegen later.
// Comments and commas serve no purpose and are ignored.

/// Tokens not used for syntax.
/// This enum is used later for more semantic parts of the compiler
/// to differentiate exactly what the data *is* and what it holds.
#[derive(Clone)]
pub enum Data {
	Id(String), // An identifier. Refers to some variable.
	Slit(String), // A string literal.
	Blit(u8), // A literal byte. Usually an ASCII character.
	Ilit(i64), // A literal number.
}

/// All kinds of tokens.
/// R means round, C means curly.
/// O means open, C means close.
#[derive(Clone)]
pub enum Tok {
	RO,
	RC,
	CO,
	CC,
	Semi, // Semicolon
	Dat(Data),
}

/// A token and its metadata
#[derive(Clone)]
pub struct Token {
pub	tok: Tok, // What exactly is it?
pub	line: u64, // Line number
pub	line0: u64,
pub	col: u64, // Column position
pub	col0: u64,
}

impl fmt::Debug for Token {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let t = self;
		f.write_str("<");
		f.write_str(match &t.tok {
			Tok::RO => "round open bracket",
			Tok::RC => "round close bracket",
			Tok::CO => "curly open bracket",
			Tok::CC => "curly close bracket",
			Tok::Semi => "semicolon",
			Tok::Dat(Data::Id(_)) => "id ",
			Tok::Dat(Data::Slit(_)) => "string ",
			Tok::Dat(Data::Blit(_)) => "byte ",
			Tok::Dat(Data::Ilit(_)) => "integer ",
		});
		match &t.tok {
			Tok::Dat(Data::Id(s)) => write!(f, "{}", s),
			Tok::Dat(Data::Slit(s)) => write!(f, "{}", s),
			Tok::Dat(Data::Blit(b)) => write!(f, "{}", b),
			Tok::Dat(Data::Ilit(n)) => write!(f, "{}", n),
			_ => Ok(()),
		};
		write!(f, "> at {}:{} to {}:{}", t.line0, t.col0, t.line, t.col)
	}
}

/// dump a token
pub fn dumptok (t: Token) {
	println!("{:?}", t);
}

/// Separate everything into debug lines
pub fn lines (input: &str) -> Vec<String> {
	let mut result = Vec::new();
	let mut this = String::new();
	let mut eofnl = false;
	for c in input.chars() {
		this.push(c);
		eofnl = false;
		if c == '\n' {
			result.push(this);
			this = String::new();
			eofnl = true;
		}
	}
	if eofnl == false {
		result.push(this);
	}
	result
}

/// word ending character?
#[inline(always)]
fn ends_word (c: char) -> bool {
	c == ' ' || c == '\t' || c == '\n' || c == '(' || c == ')' || c == ','
		|| c == ';' || c == '{' || c == '}'
}

/// escape codes
#[inline(always)]
fn escaped (c: char) -> Option<char> {
	match c.to_lowercase().nth(0).unwrap() {
		'\\' => {Some('\\')},
		'n' => {Some('\n')},
		't' => {Some('\t')},
		'r' => {Some('\r')},
		'b' => {Some('\x08')},
		'f' => {Some('\x1c')},
		'v' => {Some('\x0b')},
		'0' => {Some('\0')},
		'\'' => {Some('\'')},
		'"' => {Some('\"')},
		'x' => {panic!("This hasn't yet been implemented.\n"); None},
		'e' => {Some('\x1b')},
		'b' => {Some('\x07')},
		_ => {None}
	}
}

enum State {
	Entry, // Not in any token.
	Slash,
	CommentS, // Single line comment.
	CommentM(usize),
	CommentMSlash(usize),
	CommentMGlob(usize),
	Id(String),
	Slit(String),
	SlitEsc(String), // After a backslash
	Lit, // After a # character
	BlitEsc, // After #\
	Ilit0, // Interger literal after a 0 to determine base
	IlitD(i64), // Decimal
	IlitB(i64), // Binary
	IlitO(i64), // Octal
	IlitH(i64), // Hexidecimal
}

// One of the most difficult and yet most important things in a programming language is string
// handling. C opts for the "pretend strings don't exist" trick making strings incredibly
// difficult to program with in C. Many languages treat strings as simply sequences of bytes
// that implement a few convienient operators. This works well enough for most uses.
//
// Enter unicode.
// The world has a wonderous and diverse array of diffirent writing systems.
// The world also has a large network of computers that would generally like to communicate with
// eachother without everything getting messed up. If encodings are as diverse as writing systems,
// the result is 文字化け or mojibake. Ironically, fonts also mess this up, as my computer and
// possibly yours too is currently displaying the kanji as boxes with question marks in them.
// Unicode does have some very wierd things about it because writing systems differ wildly in many
// aspects (and even the best unicode support libraries have difficulties with top-to-bottom text),
// but that is besides the point. For now, the important thing is that there are a lot of
// characters in all different writing systems. Indeed, there are a bit less than 2 to the 21st
// power distinct graphimes and necessary control characters. This is a problem because, at the
// time the only programming language any language designer needs to know (C) was written, the
// "standard" encoding for the typical latin alphabet in America used 7 bits (and it was competing
// with an 8-bit encoding that wasn't very standard beyond stuff in the 7-bit one, and it was succeeding
// a 5-bit encoding that did cool control code trickery that lived on for an aubsurdly long time in
// aethstetics). Fortunately, this 7-bit encoding ended up being the basis for unicode after
// beating all of its competitors and, given that computers now dealt with 8-bit bytes more easily
// than 7-bit ones (or the 6-bit ones C was written on), had a free bit to mark special encoding
// stuff. This meant that byte strings could be used easily for compact text encoding (except in
// Chinese and Japanese and ancient writing systems like heiroglyphs) under UTF-8, unless you are
// microsoft, which is apparently headquartered in ancient egypt because it uses UTF-16, but not in
// an ancient greek music school because it does so poorly.
//
// Long story short, characters are 21 bits but we work with them as if they were 32-bits
// (interesting side-note, apparently rust consideres these 32-bit characters unsigned despite the
// fact that they cannot be used in arithmatic) except that these need not be actually writen and
// may just tell you how to write other characters (or there could be ligatures in which written
// characters tell you how to write other characters) except that sometimes we just want to work
// with bytes. Strings are stored as byte strings in which one character could be anywhere between
// 1 and 4 bytes long but at least you can always pick out separate characters.
//
// Python 3 pretends that these strings are actually sequences of 32-bit characters instead. This
// is slow, but so is basically everything else in python so it doesn't matter.
//
// Rust meanwhile thinks that these simple solutions are, of course, to insane for any practical
// usage. Strings cannot be indexed *properly* without a library, but if you're OK with being
// objectively wrong, I guess you *can* just use these three function calls instead.
//
// This is how we sanely index into string
// Naturally, this requires WTF-8 decoding
#[inline(always)]
fn str_index (b: &[u8], i: usize) -> char {
	let mut c: u32 = 0;
	let a = b[i];
//	println!("{}, f {}", a, (a & 0xf0) >> 4);
//	assert!(a & 0xc0 != 0x80); // if false, we are in the middle of a character
//	assert!(a & 0xf8 != 0xf8); // invalid UTF-8
	if a & 0x80 == 0x00 { // one bit
		c = a as u32;
	} else if a & 0xf0 == 0xf0 {
		c = (((a & 0x07) as u32) << 18) + (((b[i+1] & 0x3f) as u32) << 12) + (((b[i+2] & 0x3f) as u32) << 6) + (b[i+3] & 0x3f) as u32;
	} else if a & 0xf0 == 0xe0 {
		c = (((a & 0x0f) as u32) << 12) + (((b[i+1] & 0x3f) as u32) << 6) + (b[i+2] & 0x3f) as u32;
	} else { // a & 0xe0 == 0xc0
		c = (((a & 0x1f) as u32) << 6) + (b[i+1] & 0x3f) as u32;
	}
	unsafe {std::char::from_u32_unchecked(c)}
}

#[inline(always)]
fn char_len (c: char) -> usize {
	let c = c as u32;
	if c <= 0x7f {
		1
	} else if c <= 0x7ff {
		2
	} else if c <= 0xffff {
		3
	} else {
		4
	}
}

/// Separates everything into tokens
#[exec_time]
pub fn tokenise (input: &str) -> Vec<Token> {
	let mut result = Vec::new();
	let mut state = State::Entry;
	let mut line = 1u64;
	let mut line0 = line;
	let mut col = 0u64;
	let mut col0 = col;
	let mut i = 0usize;
	let b = input.as_bytes();
	let bl = b.len();
	while i < bl {
		let c = str_index(b, i);
		let l = char_len(c);
		if c == '\n' {
			line = line + 1;
			col = 0;
		} else {
			col = col + l as u64;
		}
//		print!("{}", c);
		match state {
			State::Entry => {
				if c == ' ' || c == '\t' || c == '\n' || c == ',' {col0 = col + 1;}
				else if c == '/' {state = State::Slash;}
				else if c == '"' {state = State::Slit(String::with_capacity(16));}
				else if c == '#' {state = State::Lit;}
				else if c == '0' {state = State::Ilit0;}
				else if c >= '1' && c <= '9' {state = State::IlitD(c as i64 - '0' as i64);}
				else if c == '(' {result.push(Token {tok: Tok::RO,
						line: line, line0: line0, col: col, col0: col0});
					col0 = col + 1; line0 = line;}
				else if c == ')' {result.push(Token {tok: Tok::RC,
						line: line, line0: line0, col: col, col0: col0});
					col0 = col + 1; line0 = line;}
				else if c == '{' {result.push(Token {tok: Tok::CO,
						line: line, line0: line0, col: col, col0: col0});
					col0 = col + 1; line0 = line;}
				else if c == '}' {result.push(Token {tok: Tok::CC,
						line: line, line0: line0, col: col, col0: col0});
					col0 = col + 1; line0 = line;}
				else if c == ';' {result.push(Token {tok: Tok::Semi,
						line: line, line0: line0, col: col, col0: col0});
					col0 = col + 1; line0 = line;}
				else {
					let mut s = String::with_capacity(16);
					s.push(c);
					state = State::Id(s);
				}
			}
			State::Slash => {
				if c == '/' {state = State::CommentS;}
				else if c == '*' {state = State::CommentM(1);}
				else {state = State::Id(String::from("\"")); continue;}
			},
			State::CommentS => {if c == '\n' {state = State::Entry;};},
			State::CommentM(n) => {
				if c == '/' {state = State::CommentMSlash(n);}
				else if c == '*' {state = State::CommentMGlob(n);}
			},
			State::CommentMSlash(n) => {state = State::CommentM(if c == '*' {n + 1} else {n});},
			State::CommentMGlob(n) => {
				let np = if c == '/' {n - 1} else {n};
				state = if np == 0 {State::Entry} else {State::CommentM(np)};
			},
			State::Id(mut s) => {
				if ends_word(c) {
					result.push(Token {tok: Tok::Dat(Data::Id(s)),
						line: line, line0: line0, col: col, col0: col0});
					col0 = col + 1; line0 = line;
					state = State::Entry;
					continue;
				} else {s.push(c); state = State::Id(s);}
			},
			State::Slit(mut s) => {
				if c == '\\' {state = State::SlitEsc(s);}
				else if c == '"' {
					result.push(Token {tok: Tok::Dat(Data::Slit(s)),
						line: line, line0: line0, col: col, col0: col0});
					col0 = col + 1; line0 = line;
					state = State::Entry;
				} else {s.push(c); state = State::Slit(s);}
			},
			// rust tells me that this mut s here has been moved on a previous iteration
			// it also refuses to explain further
			// there is a reason why I'm writing uxyl
/*			State::SlitEsc(mut s) => {
				match escaped(c) {
					Some(e) => {s.push(c); state = State::Slit(s);},
					_ => {error::error(&lines(input), line, line, col, col,
							"e1", &format!("Invalid escape character: {}", c));}
				}
			}, */
			State::Lit => {}, // TODO
			State::BlitEsc => {}, // TODO
			State::Ilit0 => {
				if c == 'b' || c == 'B' {
					state = State::IlitB(0);
				} else if c == 'o' || c == 'O' {
					state = State::IlitO(0);
				} else if c == '0' || c == 'd' || c == 'D' {
					state = State::IlitD(0);
				} else if c >= '1' && c <= '9' {
					state = State::IlitD('0' as i64 - c as i64);
				} else if c == 'x' || c == 'X' {
					state = State::IlitH(0);
				} else if ends_word(c) {
					result.push(Token {tok: Tok::Dat(Data::Ilit(0)),
						line: line, line0: line0, col: col, col0: col0});
					state = State::Entry;
					continue;
				} else {
					error::error(&lines(input), line, line, col, col,
						"e2", &format!("Unrecognised base character: {}", c));
				}
			},
			State::IlitB(n) => {
				match c {
					'0' => {state = State::IlitB(n * 2);},
					'1' => {state = State::IlitB(n * 2 + 1);},
					_ => {
						result.push(Token {tok: Tok::Dat(Data::Ilit(n)),
							line: line, line0: line0, col: col, col0: col0});
						state = State::Entry;
						continue;
					}
				}
			},
			State::IlitO(n) => {
				let mut a = 0;
				match c {
					'0' => {a = 0;},
					'1' => {a = 1;},
					'2' => {a = 2;},
					'3' => {a = 3;},
					'4' => {a = 4;},
					'5' => {a = 5;},
					'6' => {a = 6;},
					'7' => {a = 7;},
					_ => {
						result.push(Token {tok: Tok::Dat(Data::Ilit(n)),
							line: line, line0: line0, col: col, col0: col0});
						state = State::Entry;
						continue;
					}
				}
				state = State::IlitO(n * 8 + a);
			},
			State::IlitD(n) => {
				let mut a = 0;
				match c {
					'0' => {a = 0;},
					'1' => {a = 1;},
					'2' => {a = 2;},
					'3' => {a = 3;},
					'4' => {a = 4;},
					'5' => {a = 5;},
					'6' => {a = 6;},
					'7' => {a = 7;},
					'8' => {a = 8;},
					'9' => {a = 9;},
					_ => {
						result.push(Token {tok: Tok::Dat(Data::Ilit(n)),
							line: line, line0: line0, col: col, col0: col0});
						state = State::Entry;
						continue;
					}
				}
				state = State::IlitD(n * 10 + a);
			},
			State::IlitH(n) => {
				let mut a = 0;
				match c {
					'0' => {a = 0;},
					'1' => {a = 1;},
					'2' => {a = 2;},
					'3' => {a = 3;},
					'4' => {a = 4;},
					'5' => {a = 5;},
					'6' => {a = 6;},
					'7' => {a = 7;},
					'8' => {a = 8;},
					'9' => {a = 9;},
					'a' => {a = 10;},
					'b' => {a = 11;},
					'c' => {a = 12;},
					'd' => {a = 13;},
					'e' => {a = 14;},
					'f' => {a = 15;},
					'A' => {a = 10;},
					'B' => {a = 11;},
					'C' => {a = 12;},
					'D' => {a = 13;},
					'E' => {a = 14;},
					'F' => {a = 15;},
					_ => {
						result.push(Token {tok: Tok::Dat(Data::Ilit(n)),
							line: line, line0: line0, col: col, col0: col0});
						state = State::Entry;
						continue;
					}
				}
				state = State::IlitH(n * 16 + a);
			},
			_ => {}
		}
		i = i + l;
	}
	match state {
		State::Entry => {},
		State::CommentS => {},
		State::Slash =>{
			result.push(Token {tok: Tok::Dat(Data::Id(String::from("/"))),
				line: line, line0: line0, col: col, col0: col0});
		},
		State::CommentM(_) => {
			error::error(&lines(input), line, line, col, col,
				"e3", "Unterminated multiline comment.");
		},
		State::CommentMSlash(_) => {
			error::error(&lines(input), line, line, col, col,
				"e3", "Unterminated multiline comment.");
		},
		State::CommentMGlob(_) => {
			error::error(&lines(input), line, line, col, col,
				"e3", "Unterminated multiline comment. Did you forget to put / after the *?");
		},
		State::Id(s) => {
			result.push(Token {tok: Tok::Dat(Data::Id(s)),
				line: line, line0: line0, col: col, col0: col0});
		},
		State::Slit(s) => {
			error::error(&lines(input), line, line, col, col,
				"e4", "Unterminated string.");
		},
		State::SlitEsc(s) => {
			error::error(&lines(input), line, line, col, col,
				"e4", "Unterminated string in escape sequence.");
		},
		State::Lit => {
			error::error(&lines(input), line, line, col, col,
				"e5", "Expected literal after #.");
		},
		State::BlitEsc => {
			error::error(&lines(input), line, line, col, col,
				"e5", "Unterminated escape sequence in literal.");
		},
		State::Ilit0 => {
			result.push(Token {tok: Tok::Dat(Data::Ilit(0)),
					line: line, line0: line0, col: col, col0: col0});
		},
		State::IlitD(n) => {
			result.push(Token {tok: Tok::Dat(Data::Ilit(n)),
					line: line, line0: line0, col: col, col0: col0});
		},
		State::IlitB(n) => {
			result.push(Token {tok: Tok::Dat(Data::Ilit(n)),
					line: line, line0: line0, col: col, col0: col0});
		},
		State::IlitO(n) => {
			result.push(Token {tok: Tok::Dat(Data::Ilit(n)),
					line: line, line0: line0, col: col, col0: col0});
		},
		State::IlitH(n) => {
			result.push(Token {tok: Tok::Dat(Data::Ilit(n)),
					line: line, line0: line0, col: col, col0: col0});
		},
	}
	result
}
