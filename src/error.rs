//! Error handling

extern crate colorful;
use colorful::Color;
use colorful::Colorful;
use colorful::core::color_string::CString;
use std::io::stderr;
use std::io::Write;

// number of lines to add above and below
const context: u64 = 2;

fn ewrite (s: &str) {
	stderr().lock().write(s.as_bytes());
}

// generic reporting function
// lines, line0, line1, col0, and col1 give context
// code is of a form like "error e1" or "warning w1"
// desc is a short description of what it means
// this function *only* reports
fn report (lines: &Vec<String>, line0: u64, line1: u64, col0: u64, col1: u64,
		   code: CString, desc: &str) {
	let ctx = (line0 - context) .. (line1 + context);
	let col0 = col0 as usize;
	let col1 = col1 as usize;
	eprintln!("{}\n{}\n----------", code, desc.color(Color::Yellow));
	for i in ctx {
		let line = &lines[i as usize].as_str();
		if i < line0 || i > line1 {
			eprintln!("{}: {}", i, line);
		} else if i == line0 && i == line1 {
			eprint!("{}: ", i);
			ewrite(&line[..col0]);
			ewrite(&line[col0..col1 + 1].color(Color::Cyan).to_string());
			ewrite(&line[col1 + 1..]);
		} else if i == line0 { // and i != line1
			eprint!("{}: ", i);
			ewrite(&line[..col0]);
			ewrite(&line[col0..].color(Color::Cyan).to_string());
		} else if i == line1 {
			eprint!("{}: ", i);
			ewrite(&line[col1 + 1..].color(Color::Cyan).to_string());
			ewrite(&line[col1 + 1..]);
		} else { // line0 < i < line1
			eprint!("{}: ", i);
			ewrite(&line.color(Color::Cyan).to_string());
		}
	}
	eprintln!("----------");
}

/// report an error
pub fn error (lines: &Vec<String>, line0: u64, line1:u64, col0: u64, col1:u64,
			  code: &str, desc: &str) {
	report(lines, line0, line1, col0, col1, format!("Error {}", code).color(Color::Red), desc);
	panic!("Unrecoverable user error noticed.");
}

/// report an error without panicing
pub fn error_nonfatal (lines: &Vec<String>, line0: u64, line1:u64, col0: u64, col1:u64,
			  code: &str, desc: &str) {
	report(lines, line0, line1, col0, col1, format!("Error {}", code).color(Color::Red), desc);
}

/// provide a warning
pub fn warn (lines: &Vec<String>, line0: u64, line1:u64, col0: u64, col1:u64,
			  code: &str, desc: &str) {
	report(lines, line0, line1, col0, col1, format!("Warning {}", code).color(Color::Blue), desc);
}
